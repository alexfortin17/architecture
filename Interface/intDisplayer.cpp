#include "intDisplayer.hpp"

intDisplayer::intDisplayer()
{
    PrinterType ptype = T_iostreamPrinter;
    printerFactory pFactory = printerFactory();
    std::shared_ptr<printerInterface> printer = pFactory.createPrinter(ptype);
    m_printer = printer;
}

void intDisplayer::showToStdOut(int x)
{
    m_printer->printToStdConsole(x);
}

void intDisplayer::showToStdErr(int x)
{
    m_printer->printToErrConsole(x);
}
