#pragma once

#include "printerInterface.hpp"
#include "cPrinter.hpp"
#include "iostreamPrinter.hpp"
#include <memory>

enum PrinterType {T_iostreamPrinter, T_cPrinter};
 class printerFactory
{
    public:
        printerFactory();
        std::shared_ptr<printerInterface> createPrinter(PrinterType pType);

}; 


