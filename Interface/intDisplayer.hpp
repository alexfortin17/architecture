#include "printerInterface.hpp"
#include "printerFactory.hpp"
#include <memory>

class intDisplayer
{
    public:
        intDisplayer();
       
        void showToStdOut(int x);
        void showToStdErr(int x);

    private:
        std::shared_ptr<printerInterface> m_printer;
};
