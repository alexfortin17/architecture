#pragma once
#include <string>


class printerInterface
{
    public:
        printerInterface(){}
        virtual ~printerInterface(){};
        virtual void printToStdConsole(std::string str) = 0;
        virtual void printToStdConsole(int integer) = 0;
        virtual void printToErrConsole(std::string str) = 0;
        virtual void printToErrConsole(int integer) = 0;
 
};