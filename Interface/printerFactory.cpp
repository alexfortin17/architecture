#include "printerFactory.hpp"

printerFactory::printerFactory(){}

std::shared_ptr<printerInterface> printerFactory::createPrinter(PrinterType pType)
{
    if (pType == T_iostreamPrinter){
        std::shared_ptr<printerInterface> c =
         std::shared_ptr<printerInterface>(new cPrinter());
        return c;
    }

    else{
        std::shared_ptr<printerInterface> io =
         std::shared_ptr<printerInterface>(new iostreamPrinter());
        return io;
    }
  
}