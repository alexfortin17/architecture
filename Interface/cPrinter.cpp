#include "cPrinter.hpp"

//Constructor
cPrinter::cPrinter(){}

//Destructor
cPrinter::~cPrinter(){}

void cPrinter::printToStdConsole(std::string str)
{
    printf("%s\n", str.c_str());
}

void cPrinter::printToStdConsole(int integer)
{
    printf("%d\n", integer);
}

void cPrinter::printToErrConsole(std::string str)
{
    fprintf(stderr, "%s\n", str.c_str());
}

void cPrinter::printToErrConsole(int integer)
{
    fprintf(stderr, "%d\n", integer);
}