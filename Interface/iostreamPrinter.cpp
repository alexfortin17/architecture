#include "iostreamPrinter.hpp"

//Constructor
iostreamPrinter::iostreamPrinter(){}

//Destructor
iostreamPrinter::~iostreamPrinter(){}

void iostreamPrinter::printToStdConsole(std::string str)
{
    std::cout << str;
}

void iostreamPrinter::printToStdConsole(int integer)
{
    std::cout << integer;
}

void iostreamPrinter::printToErrConsole(std::string str)
{
    std::cerr << str;
}

void iostreamPrinter::printToErrConsole(int integer)
{
    std::cerr << integer;
}