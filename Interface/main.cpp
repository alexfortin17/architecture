#include "intDisplayer.hpp"
#include "strDisplayer.hpp"

int main()
{
    intDisplayer some_int_displayer;
    some_int_displayer.showToStdOut(5);
    strDisplayer some_str_displayer;
    some_str_displayer.showToStdOut("bla\n");
    return 0;
}