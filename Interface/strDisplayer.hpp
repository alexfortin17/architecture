#include "printerInterface.hpp"
#include "printerFactory.hpp"
#include <memory>

class strDisplayer
{
    public:
        strDisplayer();

        void showToStdOut(std::string x);
        void showToStdErr(std::string x);

    private:
        std::shared_ptr<printerInterface> m_printer;
};
