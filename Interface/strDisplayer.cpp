#include "strDisplayer.hpp"

strDisplayer::strDisplayer()
{
    PrinterType ptype = T_cPrinter;
    printerFactory pFactory = printerFactory();
    std::shared_ptr<printerInterface> printer = pFactory.createPrinter(ptype);
    m_printer = printer;
}

void strDisplayer::showToStdOut(std::string x)
{
    m_printer->printToStdConsole(x);
}

void strDisplayer::showToStdErr(std::string x)
{
    m_printer->printToErrConsole(x);
}
