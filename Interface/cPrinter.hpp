#pragma once
#include <iostream>
#include <string>
#include "printerInterface.hpp"


class cPrinter : public printerInterface
{
    public:
        cPrinter();
        ~cPrinter();

        void printToStdConsole(std::string str);
        void printToStdConsole(int integer);
        void printToErrConsole(std::string str);
        void printToErrConsole(int integer);
};
