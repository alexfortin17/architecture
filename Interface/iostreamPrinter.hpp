#pragma once
#include <iostream>
#include <string>
#include "printerInterface.hpp"


class iostreamPrinter : public printerInterface
{
    public:
        iostreamPrinter();
        ~iostreamPrinter();
        void printToStdConsole(std::string str);
        void printToStdConsole(int integer);
        void printToErrConsole(std::string str);
        void printToErrConsole(int integer);
};
